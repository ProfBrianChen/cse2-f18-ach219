public class IncrementingValues{
  public static void main (String [] args){
    int myVal = 3;
    System.out.println(++myVal);
    System.out.println(--myVal);
    System.out.println(myVal++);
    System.out.println(myVal--);
  }
}