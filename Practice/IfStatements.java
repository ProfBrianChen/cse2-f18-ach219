public class IfStatements{
  public static void main (String [] args){
    ///snippet 1
int var1 = 24;
int var2 = 13;
String word1 = "Crystal";

if( var1 > var2 + 12){
    String word2 = "palace";
    if( var2 - 6 < var1 / 4 ){
        word2 = "cave";
    }
}
System.out.println(word1 + " " + word2);
  }
  
}

   