///// CSE 02 Alana Hill 
//
import java.util.Scanner; 

public class Pyramid{
  
  public static void main(String args[]){
    Scanner scnr = new Scanner (System.in);
    double squareSideLength;
    double height;
    double volume;
    
    System.out.print("The square side of the pyramid is(input length): ");
    squareSideLength = scnr.nextDouble();
    
    System.out.print("The height of the pyramid is (input height): ");
    height = scnr.nextDouble();
    
    volume = ((squareSideLength * squareSideLength) * (height)) / 3;
    
    System.out.println("The volume inside the pyramid is: " + volume);
    
    
}
}