// Alana Hill, 09/05, CSE 002 
// My bicycle cyclometer records the time elapsed
// in seconds and the number of rotations of the front week during that time
// Your program should: 
// print the number of minutes for each trip
// print the distance for each trip in miles
// print the distance for the two trips combined 
//
public class Cyclometer {
  // main method required for every Java program
  public static void main (String[] args) {
   
    // our input data
    
    int secsTrip1=480;  //
    int secsTrip2=3220; //
    int countsTrip1=1561; //
    int countsTrip2=9037; //
    
    // our intermediate variables and output data
    double wheelDiameter=27.0, //
    PI=3.14159, //
    feetPerMile=5280, //
    inchesPerFoot=12, //
    secondsperMinute=60; //
    double distanceTrip1, distanceTrip2, totalDistance; // 
    
    // print statements
    System.out.println("Trip 1 took "+ (secsTrip1/secondsperMinute) +" minutes and had "+ countsTrip1+" counts.");
    System.out.println("Trip 2 took "+ (secsTrip2/secondsperMinute)+" minutes and had "+ countsTrip2+" counts.");
    
    // run the calculations; store the values
    // calculation here
    //
    //
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives distance in inches
    // for each count, a rotation of the wheel travels
    // the diameter in inches times PI
    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      totalDistance=distanceTrip1+distanceTrip2;
    
    // Print out the output data 
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    
  } //end of main method
} //end of class

