// Use a random number generator to select a number from 1 to 52
// Cards 1-13 represent the diamonds
// Cards 14-26 represent the clubs, then hearts, then spades



public class CardGenerator {
  public static void main(String args[]){
  // Begin main method
  String suit;
  String identity;
  
  int card = (int)(Math.random()*(51))+1;
  System.out.println(card);
 
  
  if ((card > 0) && (card <= 13)) {
    suit = "diamonds"; // 1-13 is diamonds
  }
    
    else if ((card > 13) && (card <= 26)) {
      suit = "clubs"; // 14-26 is clubs
    } 
      else if ((card > 27) && (card <= 39)) {
        suit = "hearts"; // 27-39 is hearts
      }
        else if ((card > 40) && (card <= 52)) {
          suit = "spades"; // 40-52 is spades
        }
  
  // switch statement 
  switch(card)
  {
      // case statements
      // values must be of same type of expression
    case 1: 
    case 14: 
    case 27: 
    case 40: 
    System.out.print("You picked the Ace of " + suit);
    break; // break is optional
                     
                     
     case 11:
     case 24: 
     case 37: 
     case 50:
     System.out.print("You picked the Jack of " + suit);
     break; 
      
      
    case 12:
    case 25: 
    case 38: 
    case 51: 
    System.out.print("You picked the Queen of " + suit);
    break;
      
    case 13:
    case 26:
    case 39: 
    case 52:
    System.out.print("You picked the King of " + suit);
      
   
    
      
  }
  }    
  
  }