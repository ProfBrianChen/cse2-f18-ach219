//////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{

public static void main(String args[]){
  ///prints Welcome, Class to teminal window
  System.out.println( " -----------"); 
  System.out.println( " | WELCOME |");
  System.out.println( " -----------");
  System.out.println( " ^  ^  ^  ^  ^  ^");
  System.out.println( "/ \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println( "<-a--c--h--2--1--9->");
  System.out.println( "\\ /\\ /\\ /\\ /\\ /\\ / ");
  System.out.println( " v  v  v  v  v  v");                   
}
}
