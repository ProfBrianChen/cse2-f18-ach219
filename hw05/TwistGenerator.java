// Alana Hill 
// Ask user for positive integer called "length
// If user does not provide an integer, or integer is not positive, then ask again inside while loop
// Integer is intended to indicate the user's desired twist length
// Twist is always three lines long and made of slashes and X's

import java.util.Scanner;
public class TwistGenerator {
  public static void main (String [] args){
 Scanner scnr = new Scanner(System.in);
    int length = 1;
    String junk;
    int i;
    
    
    System.out.println("Enter a positive integer");
      while (!scnr.hasNextInt()) {
        junk = scnr.next();
        System.out.println("You did not enter a positive integer, try again");
        length = scnr.nextInt();
      }
    length = scnr.nextInt();
    while (length <= 0) {
      System.out.println("Please enter a positive integer");
      length = scnr.nextInt();
    }
 
      if (length > 0) {
        System.out.print("\\");
        for (i =2; i <= length; ++i) {
          System.out.print(" ");
        }
        for (i = 4; i <= length; ++i) {
          System.out.print("//\\");
        }
        System.out.print("\n");
       for (i =2; i <= length; ++i) {
         System.out.print(" X");
       }
        for (i=3; i<= length; ++i) {
          System.out.print(" X ");
        }
      System.out.print("\n");
      for (i =1; i <= length; ++i) {
        System.out.print("/");
      }
        for (i = 2; i <= length; ++i) {
          System.out.print(" ");
        }
        for (i = 3; i <= length; ++i) {
          System.out.print(" \\//");
        }
      System.out.println("\n");
      }
 
    
      
  }
}