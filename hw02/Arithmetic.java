//////////////
//// CSE 02 Arithmetic
///
public class Arithmetic{
  // main method required for every java program
  public static void main (String[] args) {
    
    // input data
    //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;


    //Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

    double totalCostofPants; // Total Cost of Pants
    double totalCostofSweatshirts; // Total Cost of Sweatshirts
    double totalCostofBelts; // Total Cost of Belts
    
    // calculations
    totalCostofPants = numPants * pantsPrice;
    totalCostofSweatshirts = numShirts * shirtPrice;
    totalCostofBelts = numBelts * beltCost;
    // end total cost calculations
    
    // print output data
    System.out.println("Total Cost of Pants is "+totalCostofPants+"");
    System.out.println("Total Cost of Sweatshirts is "+totalCostofSweatshirts+"");
    System.out.println("Total Cost of Belts is "+totalCostofBelts+"");
    // end print output data
    
    
    double salesTaxPants; // Sales Tax on Pants
    double salesTaxShirts; // Sales Tax on Sweatshirts
    double salesTaxBelts; // Sales Tax on Belts 
    
    // calculations 
    salesTaxPants = totalCostofPants * paSalesTax;
    salesTaxShirts = totalCostofSweatshirts * paSalesTax;
    salesTaxBelts = totalCostofBelts * paSalesTax;
    // end tax calculations 
    
    // print output data 
    System.out.println ("Sales Tax of Pants is "+salesTaxPants+"");
    System.out.println ("Sales Tax of Sweatshirts is "+salesTaxShirts+"");
    System.out.println ("Sales Tax of Belts is "+salesTaxBelts+"");
    
    double totalCostofPurchases; // Total Cost of Purchases
    double totalSalesTax; // Total Sales Tax
    
    // calculations
    totalCostofPurchases = totalCostofPants + totalCostofSweatshirts + totalCostofBelts;
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts; 
    // end calculations
    
    // print output data
    System.out.println("Total Cost of Purchases Before Tax is "+totalCostofPurchases+"");
    System.out.println("Total Sales Tax is "+totalSalesTax+"");
    
    double totalCostofPurchasesPlusTax; // Total cost of purchases with tax
    
    // Final calculation
    totalCostofPurchasesPlusTax = totalCostofPurchases + totalSalesTax;
    
    // Final output data
    System.out.println("Total Cost of Purchases Including Tax is "+totalCostofPurchasesPlusTax+"");
    
  } //end of main method 
} // end of class
    
    
    
    
    
    


  
 
            
            
            
            
            